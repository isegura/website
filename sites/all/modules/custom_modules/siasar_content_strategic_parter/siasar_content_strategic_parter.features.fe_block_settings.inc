<?php
/**
 * @file
 * siasar_content_strategic_parter.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function siasar_content_strategic_parter_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-strategic_partners-block_1'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'strategic_partners-block_1',
    'i18n_block_language' => array(),
    'i18n_mode' => 0,
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'socios-estrategicos
strategic-partners',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'mothership' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'mothership',
        'weight' => 0,
      ),
      'siasar_public' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'siasar_public',
        'weight' => -7,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  return $export;
}
