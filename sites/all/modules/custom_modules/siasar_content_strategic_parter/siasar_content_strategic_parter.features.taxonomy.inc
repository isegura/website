<?php
/**
 * @file
 * siasar_content_strategic_parter.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function siasar_content_strategic_parter_taxonomy_default_vocabularies() {
  return array(
    'partner_type' => array(
      'name' => 'Partner type',
      'machine_name' => 'partner_type',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 1,
    ),
  );
}
