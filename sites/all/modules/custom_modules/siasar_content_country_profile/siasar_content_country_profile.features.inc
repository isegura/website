<?php
/**
 * @file
 * siasar_content_country_profile.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function siasar_content_country_profile_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function siasar_content_country_profile_node_info() {
  $items = array(
    'country_profile' => array(
      'name' => t('Country profile'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Country'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
