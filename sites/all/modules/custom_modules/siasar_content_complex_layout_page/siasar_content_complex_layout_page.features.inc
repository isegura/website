<?php
/**
 * @file
 * siasar_content_complex_layout_page.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function siasar_content_complex_layout_page_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function siasar_content_complex_layout_page_node_info() {
  $items = array(
    'complex_layout_page' => array(
      'name' => t('Complex layout page'),
      'base' => 'node_content',
      'description' => t('Content type for pages that require combining multiple layouts in the same page, using Paragraphs module.'),
      'has_title' => '1',
      'title_label' => t('Título'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function siasar_content_complex_layout_page_paragraphs_info() {
  $items = array(
    'image' => array(
      'name' => 'Image',
      'bundle' => 'image',
      'locked' => '1',
    ),
    'image_grid' => array(
      'name' => 'Image grid',
      'bundle' => 'image_grid',
      'locked' => '1',
    ),
    'image_section' => array(
      'name' => 'Image section',
      'bundle' => 'image_section',
      'locked' => '1',
    ),
    'image_stripe' => array(
      'name' => 'Image stripe',
      'bundle' => 'image_stripe',
      'locked' => '1',
    ),
    'main_text' => array(
      'name' => 'Main text',
      'bundle' => 'main_text',
      'locked' => '1',
    ),
    'outstanding_text' => array(
      'name' => 'Outstanding text',
      'bundle' => 'outstanding_text',
      'locked' => '1',
    ),
    'text_section' => array(
      'name' => 'Text section',
      'bundle' => 'text_section',
      'locked' => '1',
    ),
  );
  return $items;
}
