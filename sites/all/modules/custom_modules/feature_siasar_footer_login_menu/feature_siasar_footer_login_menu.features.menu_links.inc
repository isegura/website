<?php
/**
 * @file
 * feature_siasar_footer_login_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function feature_siasar_footer_login_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-logins_go-to-siasar-platform:http://staging.siasar.org.
  $menu_links['menu-logins_go-to-siasar-platform:http://staging.siasar.org'] = array(
    'menu_name' => 'menu-logins',
    'link_path' => 'http://staging.siasar.org',
    'router_path' => '',
    'link_title' => 'Go to SIASAR platform',
    'options' => array(
      'alter' => TRUE,
      'attributes' => array(
        'rel' => 'nofollow',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-logins_go-to-siasar-platform:http://staging.siasar.org',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'language' => 'en',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-logins_login-corporate-site:user.
  $menu_links['menu-logins_login-corporate-site:user'] = array(
    'menu_name' => 'menu-logins',
    'link_path' => 'user',
    'router_path' => 'user',
    'link_title' => 'Login Corporate Site',
    'options' => array(
      'alter' => TRUE,
      'attributes' => array(
        'rel' => 'nofollow',
      ),
      'item_attributes' => array(
        'id' => 'login',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-logins_login-corporate-site:user',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
    'language' => 'en',
    'menu_links_customized' => 1,
  );
  // Exported menu link: menu-logins_logout:user/logout.
  $menu_links['menu-logins_logout:user/logout'] = array(
    'menu_name' => 'menu-logins',
    'link_path' => 'user/logout',
    'router_path' => 'user/logout',
    'link_title' => 'Logout',
    'options' => array(
      'alter' => TRUE,
      'attributes' => array(
        'rel' => 'nofollow',
      ),
      'item_attributes' => array(
        'id' => 'logout',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-logins_logout:user/logout',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -41,
    'customized' => 1,
    'language' => 'en',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Go to SIASAR platform');
  t('Login Corporate Site');
  t('Logout');

  return $menu_links;
}
