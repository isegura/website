<div <?php print $id_block . $classes .  $attributes ?>>
<?php if ($block->subject): ?>
  <h2><?php print $block->subject ?></h2>
<?php endif;?>
<?php print $content ?>
</div>
