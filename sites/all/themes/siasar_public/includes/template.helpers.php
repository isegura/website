<?php


/**
 * Helper function processes Node's Main Photo field if it's set.
 */
function _siasar_public_main_photo_process(&$variables) {
  extract($variables);

  $photo_style = '';

  if (isset($field_main_photo[0])) {
    $main_image_url = image_style_url('hd', $field_main_photo[0]['uri']);
    $photo_style = 'style="background-image: url(' . $main_image_url . ');"';
  }

  $output = '
    <div class="title-header" ' . $photo_style . '>' .
      '<div class="wrapper">' .
        '<h1>' . $title . '</h1>' .
      '</div>' .
    '</div>'
  ;
  return $output;
}

/**
 * Create a URL to the Dashboard, using Country ISO2 value to build URL
 */
function _siasar_public_get_dashboard_path(&$variables) {
  if ($variables['type'] !== 'country_profile') return;

  global $language;

  $dash_base = theme_get_setting('dashboard_url');
  $output = $dash_base;

  if (isset($variables['field_country_iso2'][LANGUAGE_NONE][0]['value'])) {
    $output .= '/'
      . strtolower($variables['field_country_iso2'][LANGUAGE_NONE][0]['value'])
      . '?l=' . strtolower($language->language);
  }
  return $output;
}

/**
 * Formats date at node ful view pages
 */
function _siasar_public_format_date(&$variables) {
  $node = &$variables['elements']['#node'];
  return format_date($node->created, 'custom', 'j F, Y');
}
