
(function ($) {

  $(document).ready(function () {
    var $mainMenu = $('#block-system-main-menu');
    var $links = $mainMenu.find('> ul > li');
    var timerHoverMenu;

    $links.on('mouseenter tap', addHoverClassToLink);

    $links.on('mouseleave', setTimerToRemoveHoverClassToLink);

    function addHoverClassToLink(e) {
      if (timerHoverMenu) clearTimeout(timerHoverMenu);
      closeAllOtherMenus(e.currentTarget);

      if (e.type == 'tap' && !this.classList.contains('tap')) {
        $links.removeClass('tapped');
        this.classList.add('tapped');
        e.preventDefault();
      }
      if (!this.classList.contains('hover')) {
        this.classList.add('hover');
      }
    }

    function setTimerToRemoveHoverClassToLink(e) {
      timerHoverMenu = setTimeout(function () {
        removeHoverClassToLink(e.currentTarget);
      }, 1000);
    }

    function removeHoverClassToLink(target) {
      target.classList.remove('hover');
    }

    function closeAllOtherMenus(notThis) {
      var $targetMenuLinks = $links.not(notThis);

      $targetMenuLinks.removeClass('hover');
    }

  });

})(jQuery);
